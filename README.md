# Topological  Data  Analysis  (TDA)

## Abstract

Topological Data Analysis (TDA) has shown to be a powerful tool that provides insight in
the structure of data. Techniques of TDA, such as persistent homology, allow the study of
qualitative features of data that persist across different predefined threshold values.
The features can then be summarized in persistence diagrams for representation and further
analysis. Nevertheless, as TDA is a recent field of study, there hasn’t been given too
much effort in comparing different standard Machine Learning (ML) methods with techniques
in this field. In this work, we want to overcome this issue by comparing two well known ML
approaches with two different multi-scale kernels for persistence diagrams. We analyse
these methods using a handwritten data set, as this has been studied thoroughly in the ML
community.

![picture](data/plots/accuracy_moreplots.png)

Figure 1: Comparison of the TDA and ML approaches.
TDA: sliced Wasserstein kernel (SW) [1] and scale-space kernel [2];
ML: Convolutional Neural Network (CNN) and k-Nearest Neighbors (k-NN).

\[1\] Mathieu Carrière, Marco Cuturi, and Steve Oudot. Sliced Wasserstein Kernel for
Persistence Diagrams. 2017. arXiv:1706.03358 [cs.CG].

\[2\] Jan Reininghaus et al. A Stable Multi-Scale Kernel for Topological Machine Learning.
2014. arXiv:1412.6821 [stat.ML].

## Dependencies
This project requires the pershombox package, which can not be installed using PyPI.
See ![https://github.com/c-hofer/tda-toolkit](https://github.com/c-hofer/tda-toolkit)
for installation instruction.
The remaining packages can be installed via the following command.

```
    pip install -U -r requirements.txt
```

## Running the Experiments

```
    python src/compute_pdgms.py
    python src/cross_validation.py
    python src/make_plots.py
```

## Authors
* **Sebastian Dobrzynski**
* **Daniel Gonzalez**
* **André Schulze**